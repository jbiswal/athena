#include "MdtCalibTools/DCSLFitterTool.h"
#include "MdtCalibTools/QuasianalyticLineReconstructionTool.h"
#include "MdtCalibTools/MdtCalibTool.h"
#include "MdtCalibTools/SimpleMdtSegmentSelectorTool.h"
#include "MdtCalibTools/SimplePatternSelectorTool.h"
#include "MdtCalibTools/MdtCalibNtupleMakerTool.h"


using namespace MuonCalib;

DECLARE_COMPONENT( DCSLFitterTool )
DECLARE_COMPONENT( QuasianalyticLineReconstructionTool )
DECLARE_COMPONENT( MdtCalibTool )
DECLARE_COMPONENT( SimpleMdtSegmentSelectorTool )
DECLARE_COMPONENT( SimplePatternSelectorTool )
DECLARE_COMPONENT( MdtCalibNtupleMakerTool )

